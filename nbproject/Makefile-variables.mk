#
# Generated - do not edit!
#
# NOCDDL
#
CND_BASEDIR=`pwd`
CND_BUILDDIR=build
CND_DISTDIR=dist
# Release configuration
CND_PLATFORM_Release=GNU-Linux-x86
CND_ARTIFACT_DIR_Release=dist/Release/GNU-Linux-x86
CND_ARTIFACT_NAME_Release=assignment2
CND_ARTIFACT_PATH_Release=dist/Release/GNU-Linux-x86/assignment2
CND_PACKAGE_DIR_Release=dist/Release/GNU-Linux-x86/package
CND_PACKAGE_NAME_Release=assignment2.tar
CND_PACKAGE_PATH_Release=dist/Release/GNU-Linux-x86/package/assignment2.tar
# Debug configuration
CND_PLATFORM_Debug=GNU-Linux-x86
CND_ARTIFACT_DIR_Debug=dist/Debug/GNU-Linux-x86
CND_ARTIFACT_NAME_Debug=assignment2
CND_ARTIFACT_PATH_Debug=dist/Debug/GNU-Linux-x86/assignment2
CND_PACKAGE_DIR_Debug=dist/Debug/GNU-Linux-x86/package
CND_PACKAGE_NAME_Debug=assignment2.tar
CND_PACKAGE_PATH_Debug=dist/Debug/GNU-Linux-x86/package/assignment2.tar
# Windows_Debug configuration
CND_PLATFORM_Windows_Debug=MinGW-i686-Linux-x86
CND_ARTIFACT_DIR_Windows_Debug=dist/Windows_Debug/MinGW-i686-Linux-x86
CND_ARTIFACT_NAME_Windows_Debug=assignment2.exe
CND_ARTIFACT_PATH_Windows_Debug=dist/Windows_Debug/MinGW-i686-Linux-x86/assignment2.exe
CND_PACKAGE_DIR_Windows_Debug=dist/Windows_Debug/MinGW-i686-Linux-x86/package
CND_PACKAGE_NAME_Windows_Debug=assignment2.tar
CND_PACKAGE_PATH_Windows_Debug=dist/Windows_Debug/MinGW-i686-Linux-x86/package/assignment2.tar
# Windows_Release configuration
CND_PLATFORM_Windows_Release=MinGW-i686-Linux-x86
CND_ARTIFACT_DIR_Windows_Release=dist/Windows_Release/MinGW-i686-Linux-x86
CND_ARTIFACT_NAME_Windows_Release=assignment2.exe
CND_ARTIFACT_PATH_Windows_Release=dist/Windows_Release/MinGW-i686-Linux-x86/assignment2.exe
CND_PACKAGE_DIR_Windows_Release=dist/Windows_Release/MinGW-i686-Linux-x86/package
CND_PACKAGE_NAME_Windows_Release=assignment2.tar
CND_PACKAGE_PATH_Windows_Release=dist/Windows_Release/MinGW-i686-Linux-x86/package/assignment2.tar
# Release32 configuration
CND_PLATFORM_Release32=GNU-Linux-x86
CND_ARTIFACT_DIR_Release32=dist/Release32/GNU-Linux-x86
CND_ARTIFACT_NAME_Release32=assignment2
CND_ARTIFACT_PATH_Release32=dist/Release32/GNU-Linux-x86/assignment2
CND_PACKAGE_DIR_Release32=dist/Release32/GNU-Linux-x86/package
CND_PACKAGE_NAME_Release32=assignment2.tar
CND_PACKAGE_PATH_Release32=dist/Release32/GNU-Linux-x86/package/assignment2.tar
#
# include compiler specific variables
#
# dmake command
ROOT:sh = test -f nbproject/private/Makefile-variables.mk || \
	(mkdir -p nbproject/private && touch nbproject/private/Makefile-variables.mk)
#
# gmake command
.PHONY: $(shell test -f nbproject/private/Makefile-variables.mk || (mkdir -p nbproject/private && touch nbproject/private/Makefile-variables.mk))
#
include nbproject/private/Makefile-variables.mk
