/* 
 * File:   CSimulator.cpp
 * Author: Mihail Atanassov, s1018353
 * 
 * Created on February 9, 2013, 6:41 PM
 */

#include <cstdlib>

#include "include/CSimulator.h"

using namespace std;

CSimulator::CSimulator( ) :
m_nAssoc( 0 ),
m_nNumRead( 0 ),
m_nNumWrite( 0 ),
m_strInFile( NULL ) { }

CSimulator::~CSimulator( ) { }

bool CSimulator::init( int &argc, char** argv )
{
	switch ( init_( argc, argv ) )
	{
		case INIT_OK:
			return true;
		case INIT_NOFILE:
			printf( "No input file.\n" );
			return false;
		case INIT_BAD_ASSOC:
			printf( "Invalid associativity.\n\
Accepted numbers are: 1, 2, 4, 8, 16.\n" );
			return false;
		case INIT_BAD_SETNUM:
			printf( "The number of sets is not a power of 2.\n");
			return false;
		case INIT_OPT_WO_ARG:
			printf( "Option without an argument.\n" );
			return false;
		case INIT_UNKNOWN_OPT:
			fprintf( stderr, "Unknown option.\n" );
			return false;
		case INIT_HELP:
			exit( EXIT_SUCCESS );
		default:
			return false;
	}
}

int CSimulator::init_( int &argc, char** argv )
{
	const char * const optStr = ":hw:s:";
	int c = 0;
	while ( ( c = getopt( argc, argv, optStr ) ) != -1 )
	{
		switch ( c )
		{
			case 'h':
				printf( "Usage:\n\
assignment2 -s <associativity> <input-file>\n\
Accepted associativity numbers are: 1, 2, 4, 8, 16.\n\
Datafile format:\tR 0123abcd45de\n\t\t\tW 0123abcd45de\n" );
				return INIT_HELP;
			case 'w': /* number of ways */
				m_nAssoc =
					(uint8_t) strtoumax( optarg, NULL, 10 );
				break;
			case 's': /* number of sets */
				m_nSets =
					(uint32_t) strtoumax( optarg,
							 NULL,
							 10 );
				break;
			case ':':
				return INIT_OPT_WO_ARG;
				break;
			case '?':
				return INIT_UNKNOWN_OPT;
				break;
			default:
				break;
		}
	}

	switch ( m_nAssoc ) /* check if associativity is valid */
	{
		case 1:
		case 2:
		case 4:
		case 8:
		case 16:
			break; /* all OK */
		default:
			return INIT_BAD_ASSOC; /* bad associativity value */
	}

	if( pow( 2, (uint32_t) log2(m_nSets)) != m_nSets)
		return INIT_BAD_SETNUM;
	
	/* Opts are parsed, now we need the input filename */
	if ( argv[optind] == NULL )
	{
		return INIT_NOFILE;
	}
	m_strInFile = argv[optind];
	return INIT_OK;
}

bool CSimulator::readFile( )
{
	/* gcc & the C way: 5 times faster than using stringstream etc. */
	FILE* fh = NULL;
	if ( ( fh = fopen( m_strInFile, "r" ) ) == NULL )
		return false; /* File does not exist? */
	char line[16];
	uint64_t addr = 0;
	bool rw = READ;
	while ( fgets( line, sizeof line, fh ) != NULL ) /* parse each line */
	{
		if ( line[0] == 'W' ) /* read or write? */
		{
			rw = WRITE;
			++m_nNumWrite;
		}
		else
		{
			rw = READ;
			++m_nNumRead;
		}

#if defined __x86_64__ | WIN64 == 1 /* get address */
		if ( !sscanf( line + 2, "%12lx", &addr ) )
		{
			fclose( fh );
			return false; /* bad data */
		}
#else
		if ( !sscanf( line + 2, "%12llx", &addr ) )
		{
			fclose( fh );
			return false; /* bad data */
		}
#endif
		m_data.push_back( std::make_tuple( addr, rw ) ); /* store it */
	}
	fclose( fh );
	if ( m_data.empty( ) )
		return false; /* the file did not have anything in it */
	return true;
}

//<editor-fold defaultstate="collapsed" desc="Getters">

const uint64_t CSimulator::getDataSize( ) const
{
	return m_data.size( );
}

const char* CSimulator::getInFile( ) const
{
	return m_strInFile;
}

const uint8_t CSimulator::getAssoc( ) const
{
	return m_nAssoc;
}

const results_t CSimulator::getResults( ) const
{
	return m_results;
}
//</editor-fold>

void CSimulator::run( )
{
	/* Simulation for a set-associative cache -> LRU replacement logic
	 * When simulating direct-mapped cache -> LRU is simply the only tag
	 */
	uint64_t addr = 0, cacheAddr = 0, cacheTag = 0;
	bool rw = READ;
	double numReadMiss = 0.0L, numWriteMiss = 0.0L;
	/* the width of the bit ield that will address into the cache */
	uint32_t cacheAddrW = (uint32_t) log2( m_nSets );

	for ( m_dataIterator = m_data.begin( );
	 m_dataIterator != m_data.end( );
	 ++m_dataIterator )
	{
		tie( addr, rw ) = *m_dataIterator;
		cacheAddr = ( addr >> BYTEOFFSET ) & ( m_nSets - 1 );
		cacheTag = addr >> ( BYTEOFFSET + cacheAddrW );
		m_cacheIt = m_cache.find( cacheAddr );
		if ( m_cacheIt == m_cache.end( ) )/* line not used before */
		{
			if ( rw == READ )
				++numReadMiss;
			else
				++numWriteMiss;
			list<uint64_t> tagList;
			tagList.push_front( cacheTag );
			m_cache.insert( pair< uint64_t, list<uint64_t> >(
								 cacheAddr,
								 tagList ) );
		}
		else /* line already exists */
		{
			if ( !matchTag( m_cacheIt->second, cacheTag ) )
			{
				/* no tag matches */
				if ( rw == READ )
					++numReadMiss;
				else
					++numWriteMiss;
				addTag( m_cacheIt->second, cacheTag );
			}
			else /* tag match */
				updateTag( m_cacheIt->second, cacheTag );
		}
	}
	/* pass complete, update results array */
	get<0>( m_results ) = m_nSets * 32 * m_nAssoc / 1024.0L;
	get<1>( m_results ) = numReadMiss * 100.0L / m_nNumRead;
	get<2>( m_results ) = numWriteMiss * 100.0L / m_nNumWrite;
	get<3>( m_results ) = ( numReadMiss + numWriteMiss ) *
		100.0L / ( m_nNumRead + m_nNumWrite );
}

/* @return: false on no match, true on match */
bool CSimulator::matchTag( list<uint64_t>& cachedTags, uint64_t newTag ) const
{
	for ( list<uint64_t>::iterator tagsIt = cachedTags.begin( );
	 tagsIt != cachedTags.end( );
	 ++tagsIt )
	{
		if ( *tagsIt == newTag )
			return true;
	}
	return false;
}

/* @pre: newTag is NOT present in cachedTags
 * @post: newTag is the most recently used tag
 */
void CSimulator::addTag( list<uint64_t>& cachedTags, uint64_t newTag )
{
	//first, remove the LRU tag if the max amount of tags is present
	if ( cachedTags.size( ) == m_nAssoc )
		cachedTags.pop_back( );
	//and add the new one up front
	cachedTags.push_front( newTag );
}

/* @pre: newTag is present in cachedTags
 * @post: newTag is the most recently used tag
 */
void CSimulator::updateTag( list<uint64_t>& cachedTags, uint64_t newTag )
{
	for ( list<uint64_t>::iterator tagsIt = cachedTags.begin( );
	 tagsIt != cachedTags.end( );
	 ++tagsIt )
	{
		if ( *tagsIt == newTag )
		{
			cachedTags.erase( tagsIt );
			break;
		}
	}
	cachedTags.push_front( newTag );
}
