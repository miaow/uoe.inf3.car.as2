/* 
 * File:   CSimulator.h
 * Author: Mihail Atanassov, s1018353
 *
 * Created on February 9, 2013, 6:41 PM
 */

#ifndef CSIMULATOR_H
#define	CSIMULATOR_H

using namespace std;

/* includes */
#include<unistd.h>	/* getopt */
#include<inttypes.h>	/* (u)intXX_t */
#include<cstdio>	/* printf */
#include<vector>	/* std::vector */
#include<tuple>		/* std::tuple */
#include<map>		/* std::map */
#include<list>		/* std::list */
#include<cmath>		/* log2() */

/* return codes */
#define INIT_OK			0
#define INIT_NOFILE		1
#define INIT_BAD_ASSOC		2
#define INIT_OPT_WO_ARG		3
#define INIT_UNKNOWN_OPT	4
#define INIT_HELP		5

/* whether we have a read or a write access */
#define WRITE			true
#define READ			false

/* cache-related things */
#define BYTEOFFSET		5
#define CACHE_MINSIZE		4096
#define CACHE_MAXSIZE		65536

/* the type that will hold the simulation results:
 * cache size (KiB), read miss rate, write miss rate, total miss rate 
 */
typedef tuple<uint32_t, double, double, double> results_t;

class CSimulator
{
private:
	/* data members */
	uint8_t						m_nAssoc;
	uint32_t					m_nSets;
	const char					*m_strInFile;
	results_t					m_results[5];
	/* Let the space-wasting begin */
	vector< tuple<uint64_t, bool> >			m_data;
	vector< tuple<uint64_t, bool> >::iterator	m_dataIterator;
	/* Key: the cache address (which is unique and maps 1 to 1)
	 * Value: the cache tag list, used for matching the contents
	 */
	map< uint64_t, list<uint64_t> >			cache;
	map< uint64_t, list<uint64_t> >::iterator	cacheIt;
	uint32_t					m_nNumRead, 
							m_nNumWrite;

public:
		CSimulator( );
	virtual ~CSimulator( );
	bool	init(		int&,			char** );
	bool	readFile( );
	void	run( );
	
	//<editor-fold defaultstate="collapsed" desc="Getters">
	const char* getInFile( ) const;
	const uint64_t getDataSize( ) const;
	const uint8_t getAssoc( ) const;
	const results_t getResults( int ) const;
	//</editor-fold>

private:
	int	init_(		int&,			char** );
	bool	matchTag(	list<uint64_t>&,	uint64_t ) const;
	void	addTag(		list<uint64_t>&,	uint64_t );
	void	updateTag(	list<uint64_t>&,	uint64_t );
};

#endif	/* CSIMULATOR_H */
