/* 
 * File:   CSimulator.h
 * Author: mihail
 *
 * Created on February 9, 2013, 6:41 PM
 */

#ifndef CSIMULATOR_H
#define	CSIMULATOR_H

/* includes */
#include<unistd.h>
#include<stdio.h>
#include<inttypes.h>
#include<cstdio>
#include<vector>
#include<tuple>
#include<unordered_map>
#include<map>
/* return codes */
#define INIT_OK			0
#define INIT_NOFILE		1
#define INIT_BAD_SIMTYPE	2
#define INIT_BAD_CMDLINE	3
#define INIT_OPT_WO_ARG		4
#define INIT_UNKNOWN_OPT	5
#define INIT_HELP		6

#define SIM_STATIC_ALL		1
#define SIM_STATIC_NONE		0
#define SIM_STATIC_PGP		2
#define SIM_DYNAMIC		3
#define SIM_UNDEF		-1


class CSimulator
{
private:
	/* data members */
	int m_nSimType;
	const char *m_strSimType, *m_strInFile;
	FILE *fh;
	std::vector<std::tuple<int, bool>> m_data;
	
public:
	CSimulator( );
	virtual ~CSimulator( );
	bool init( int&, char** );
	bool readFile( );
	double run();
	const char* getSimType() const;
	const char* getInFile() const;
	
	
private:
	inline void printHelp();
	/* make the tester our friend, so he can get the init_ function */
	// encapsulation vs testing....
	friend class CSimulator_test;
	int init_( int&, char** );
	double runStatic( bool );
	double runStaticP();
	double runDynamic();
};

#endif	/* CSIMULATOR_H */

