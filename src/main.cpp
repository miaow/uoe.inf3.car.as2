/* 
 * File:   main.cpp
 * Author: Mihail Atanassov, s1018353
 *
 * Created on February 9, 2013, 6:40 PM
 */

#include <cstdlib>

#include "include/CSimulator.h"

using namespace std;

int main( int argc, char** argv )
{
	CSimulator sim;
	if ( !sim.init( argc, argv ) )
	{
		printf( "[Simulator]\tError during init, exiting.\n" );
		return EXIT_FAILURE;
	}

	if ( !sim.readFile( ) )
	{
		printf( "[Simulator]\tError reading file or file empty.\n" );
		return EXIT_FAILURE;
	}
	sim.run( );

	double  cacheSize = 0.0L, 
		readMiss = 0.0L, 
		writeMiss = 0.0L, 
		totalMiss = 0.0L;
	const char* filename = sim.getInFile();
	const uint64_t dataSize = sim.getDataSize( );
	const int assoc = sim.getAssoc( );
	printf( "+------------------------------------------------------+\n" );
	if ( assoc == 1 )
		printf(
		 "|                 Direct-mapped Cache                  |\n" );
	else
		printf(
		 "|             %2d-way Set-Associative Cache             |\n",
		 assoc );
	printf("+------------------------------------------------------+\n");
	printf("|               %24s               |\n", filename);
	printf( "+------------+-------------+-------------+-------------+\n" );
	printf( "| Cache Size | R Miss Rate | W Miss Rate |    Total    |\n" );
	printf( "+------------+-------------+-------------+-------------+\n" );
	tie( cacheSize, readMiss, writeMiss, totalMiss ) =
		sim.getResults();
	if ( readMiss == 0 && writeMiss == 0 )
	{
		/* It's not technically possible to have 0 misses */
		printf(
		 "[FAILURE]\tThis program's buggy, delete it NOW!\n" );
		return EXIT_FAILURE;
	}
	printf(
		"|  %4.0lf KiB  |  %7.4lf %%  |  %7.4lf %%  |  %7.4lf %%  |\n",
		cacheSize,
		readMiss,
		writeMiss,
		totalMiss );
	printf(
		"+------------+-------------+-------------+-------------+\n" );
	return EXIT_SUCCESS;
}
